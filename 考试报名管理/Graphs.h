﻿#include<graphics.h>
#include<stdio.h>
#include<conio.h>
#include<Windows.h>
#include<iostream>
#include<fstream>
#include"Struct.h"
#pragma comment(lib,"Imm32.lib")
using namespace std;
extern LinkList stu;
extern int length;
void GetIMEString(HWND hWnd, string& str);
void Input(string &name, int x, int y, MOUSEMSG k, IMAGE bg_img, string image_road);
void Input(HWND hwnd,string& name, int x, int y,  IMAGE bg_img, string image_road);
void Form2(int w,int h);
void Form3(int w,int h);
void Form4(int w,int h);
void Form5(int w,int h);
void Form1(int w,int h)
{
	int i=h/100;int height=i*100;
	HWND hwnd=initgraph(w, height);
	SetWindowTextA(hwnd, "考试报名管理系统");
	IMAGE background;
	loadimage(&background, "images/001.jpg");
	putimage(0,0,&background);
	settextstyle(30, 0, _T("华文楷体"));
	setbkmode(TRANSPARENT);
	rectangle(w*0.3,height*0.1,w*0.65,height*0.15);
	outtextxy(w*0.4,height*0.11,_T("☆添加功能☆"));
	rectangle(w*0.3,height*0.2,w*0.65,height*0.25);
	outtextxy(w*0.4,height*0.21,_T("☆删除功能☆"));
	rectangle(w*0.3,height*0.3,w*0.65,height*0.35);
	outtextxy(w*0.4,height*0.31,_T("☆查找功能☆"));
	rectangle(w*0.3,height*0.4,w*0.65,height*0.45);
	outtextxy(w*0.4,height*0.41,_T("☆清空功能☆"));
	rectangle(w*0.3,height*0.5,w*0.65,height*0.55);
	outtextxy(w*0.4,height*0.51,_T("☆排序功能☆"));
	rectangle(w*0.3,height*0.6,w*0.65,height*0.65);
	outtextxy(w*0.4,height*0.61,_T("☆统计功能☆"));
	rectangle(w*0.3,height*0.7,w*0.65,height*0.75);
	outtextxy(w*0.4,height*0.71,_T("☆退出系统☆"));
	MOUSEMSG k;
	while(true)
	{
		k=GetMouseMsg();


		if(k.x>=w*0.3&&k.x<=w*0.65&&k.y>=height*0.1&&k.y<=height*0.15)
		{
			setlinecolor(RED);
			rectangle(w*0.3,height*0.1,w*0.65,height*0.15);
			if(k.uMsg==WM_LBUTTONDOWN) 
				Form2(w,h);
		}
		if(!(k.x>=w*0.3&&k.x<=w*0.65&&k.y>=height*0.1&&k.y<=height*0.15))
		{
			setlinecolor(WHITE);
			rectangle(w*0.3,height*0.1,w*0.65,height*0.15);
		}
		if(k.x>=w*0.3&&k.x<=w*0.65&&k.y>=height*0.3&&k.y<=height*0.35)
		{
			setlinecolor(RED);
			rectangle(w*0.3,height*0.3,w*0.65,height*0.35);
			if(k.uMsg==WM_LBUTTONDOWN) 
				Form3(w,h);
		}
		if(!(k.x>=w*0.3&&k.x<=w*0.65&&k.y>=height*0.3&&k.y<=height*0.35))
		{
			setlinecolor(WHITE);
			rectangle(w*0.3,height*0.3,w*0.65,height*0.35);
		}
		if(k.x>=w*0.3&&k.x<=w*0.65&&k.y>=height*0.2&&k.y<=height*0.25)
		{
			setlinecolor(RED);
			rectangle(w*0.3,height*0.2,w*0.65,height*0.25);
			if(k.uMsg==WM_LBUTTONDOWN) 
				Form4(w,h);
		}
		if(!(k.x>=w*0.3&&k.x<=w*0.65&&k.y>=height*0.2&&k.y<=height*0.25))
		{
			setlinecolor(WHITE);
			rectangle(w*0.3,height*0.2,w*0.65,height*0.25);
		}
		if(k.x>=w*0.3&&k.x<=w*0.65&&k.y>=height*0.4&&k.y<=height*0.45)
		{
			setlinecolor(RED);
			rectangle(w*0.3,height*0.4,w*0.65,height*0.45);
			if(k.uMsg==WM_LBUTTONDOWN) 
			{
				ifstream in;in.open("data.txt");
				int n;in>>n;in.close();read(stu,n);
				MessageBox(hwnd,"确认清空！！！","提示",MB_OK);
				ClearList_L(stu);
				ofstream outfile;
				length=ListLength(stu);
				write(stu,length);
				ofstream output;
				output.open("data.txt");
				output<<length;
				output.close();
			}
		}
		if(!(k.x>=w*0.3&&k.x<=w*0.65&&k.y>=height*0.4&&k.y<=height*0.45))
		{
			setlinecolor(WHITE);
			rectangle(w*0.3,height*0.4,w*0.65,height*0.45);
		}
		if(k.x>=w*0.3&&k.x<=w*0.65&&k.y>=height*0.5&&k.y<=height*0.55)
		{
			setlinecolor(RED);
			rectangle(w*0.3,height*0.5,w*0.65,height*0.55);
			if(k.uMsg==WM_LBUTTONDOWN)
			{
				ifstream in;in.open("data.txt");
				int n;in>>n;in.close();read(stu,n);
				SortList(stu);
				ofstream outfile;
				length=ListLength(stu);
				write(stu,length);
				ofstream output;
				output.open("data.txt");
				output<<length;
				output.close();
				MessageBox(hwnd,"排序成功！！！","提示",MB_OK);
			}
		}
		if(!(k.x>=w*0.3&&k.x<=w*0.65&&k.y>=height*0.5&&k.y<=height*0.55))
		{
			setlinecolor(WHITE);
			rectangle(w*0.3,height*0.5,w*0.65,height*0.55);
		}
		if(k.x>=w*0.3&&k.x<=w*0.65&&k.y>=height*0.6&&k.y<=height*0.65)
		{
			setlinecolor(RED);
			rectangle(w*0.3,height*0.6,w*0.65,height*0.65);
			if(k.uMsg==WM_LBUTTONDOWN) 
				Form5(w,h);
		}
		if(!(k.x>=w*0.3&&k.x<=w*0.65&&k.y>=height*0.6&&k.y<=height*0.65))
		{
			setlinecolor(WHITE);
			rectangle(w*0.3,height*0.6,w*0.65,height*0.65);
		}
		if(k.x>=w*0.3&&k.x<=w*0.65&&k.y>=height*0.7&&k.y<=height*0.75)
		{
			setlinecolor(RED);
			rectangle(w*0.3,height*0.7,w*0.65,height*0.75);
			if(k.uMsg==WM_LBUTTONDOWN)
				exit(0);
		}
		if(!(k.x>=w*0.3&&k.x<=w*0.65&&k.y>=height*0.7&&k.y<=height*0.75))
		{
			setlinecolor(WHITE);
			rectangle(w*0.3,height*0.7,w*0.65,height*0.75);
		}
		


	}
	
}
void Form2(int w,int h)//添加
{
	ifstream in;in.open("data.txt");
	int n;in>>n;in.close();read(stu,n);
	int i=h/100 ,nd;char nb[10];
	int height=i*100;student a;
	HWND hwnd=initgraph(w, height);
	SetWindowTextA(hwnd, "考试报名管理系统");
	IMAGE background;
	loadimage(&background,"images/002.jpg");
	putimage(0,0,&background);
	settextstyle(30, 0, _T("华文楷体"));
	setbkmode(TRANSPARENT);
	char sub[5][50]={"Java课程设计","数据结构","大学英语","概率论","离散数学"};
	outtextxy(w*0,height*0.01,_T("所有课程："));
	outtextxy(w*0.01,height*0.11,sub[0]);
	outtextxy(w*0.01,height*0.21,sub[1]);
	outtextxy(w*0.01,height*0.31,sub[2]);
	outtextxy(w*0.01,height*0.41,sub[3]);
	outtextxy(w*0.01,height*0.51,sub[4]);
	outtextxy(w*0.3,height*0.11,_T("学号："));
	outtextxy(w*0.3,height*0.21,_T("姓名："));
	outtextxy(w*0.3,height*0.31,_T("性别："));
	outtextxy(w*0.3,height*0.41,_T("学科："));
	rectangle(w*0.1,height*0.8,w*0.25,height*0.85);
	outtextxy(w*0.15,height*0.81,_T("返回"));
	rectangle(w*0.7,height*0.8,w*0.85,height*0.85);
	outtextxy(w*0.75,height*0.81,_T("确定"));
	/*Sleep(100);
	InputBox(a.num, 10, _T("请输入学号:"),false);
	outtextxy(w*0.4, height*0.11, a.num);
	Sleep(100);
	InputBox(a.name, 50, _T("请输入姓名:"),false);
	outtextxy(w*0.4, height*0.21, a.name);
	Sleep(100);
	InputBox(a.gender, 6, _T("请输入性别:"),false);
	outtextxy(w*0.4, height*0.31, a.gender);
	Sleep(100);*/
	/*InputBox(a.subject, 5, _T("请输入学科:"),false);
	outtextxy(w*0.4, height*0.41, a.subject);
	Sleep(100);
	InputBox(nb, 10, _T("请输入位置:"),false);
	outtextxy(w*0.4, height*0.51, nb);
	nd=atoi(nb);*/
	
	MOUSEMSG k;
	while(true)
	{
		k=GetMouseMsg();
		if (k.x >= w * 0.4 && k.x <= w * 0.6 && k.y >= height * 0.1 && k.y <= height * 0.15)
		{
			if (k.uMsg == WM_LBUTTONDOWN)
			{
				string b;
				/*setlinecolor(RED);
				rectangle(w * 0.4, height * 0.1, w * 0.6, height * 0.15);*/
				Input(b, w * 0.4, height * 0.11,k,  background, "images/002.jpg");
				strncpy(a.num, b.c_str(), b.length() + 1);
			}
			
			
		}
		/*if (!(k.x >= w * 0.4 && k.x <= w * 0.6 && k.y >= height * 0.1 && k.y <= height * 0.15))
		{
			setlinecolor(WHITE);
			rectangle(w * 0.4, height * 0.1, w * 0.6, height * 0.15);
		}*/
		if (k.x >= w * 0.4 && k.x <= w * 0.6 && k.y >= height * 0.2 && k.y <= height * 0.25)
		{
			if (k.uMsg == WM_LBUTTONDOWN)
			{
				string b;
				/*setlinecolor(RED);
				rectangle(w * 0.4, height * 0.2, w * 0.6, height * 0.25);*/
				Input(hwnd, b, w * 0.4, height * 0.2, background, "images/002.jpg");
				strncpy(a.name, b.c_str(), b.length() + 1);
			}
			
		}
		/*if (!(k.x >= w * 0.4 && k.x <= w * 0.6 && k.y >= height * 0.2 && k.y <= height * 0.25))
		{
			setlinecolor(WHITE);
			rectangle(w * 0.4, height * 0.2, w * 0.6, height * 0.25);
		}*/
		if (k.x >= w * 0.4 && k.x <= w * 0.6 && k.y >= height * 0.3 && k.y <= height * 0.35)
		{
			if (k.uMsg == WM_LBUTTONDOWN)
			{
				string b;
				/*setlinecolor(RED);
				rectangle(w * 0.4, height * 0.3, w * 0.6, height * 0.35);*/
				Input(hwnd, b, w * 0.4, height * 0.3,  background, "images/002.jpg");
				strncpy(a.gender, b.c_str(), b.length() + 1);
			}
			
		}
		/*if (!(k.x >= w * 0.4 && k.x <= w * 0.6 && k.y >= height * 0.3 && k.y <= height * 0.35)) {
			setlinecolor(WHITE);
			rectangle(w * 0.4, height * 0.3, w * 0.6, height * 0.35);
		}*/


		if(k.x>=w*0&&k.x<=w*0.2&&k.y>=height*0.11&&k.y<=height*0.15)
		{
			setlinecolor(RED);
			rectangle(w*0,height*0.11,w*0.2,height*0.15);
			if(k.uMsg==WM_LBUTTONDOWN) 
			{
				clearrectangle(w*0.4,height*0.41,w*0.5,height*0.45);
				putimage(w*0.4,height*0.41, w*0.4, height*0.3, &background, w*0.4, height*0.41);
				outtextxy(w*0.4, height*0.41, sub[0]);
				strcpy(a.subject,sub[0]);
			}
		}
		if(!(k.x>=w*0&&k.x<=w*0.2&&k.y>=height*0.11&&k.y<=height*0.15))
		{
			setlinecolor(TRANSPARENT);
			rectangle(w*0,height*0.11,w*0.2,height*0.15);
		}
		if(k.x>=w*0&&k.x<=w*0.2&&k.y>=height*0.21&&k.y<=height*0.25)
		{
			setlinecolor(RED);
			rectangle(w*0,height*0.21,w*0.2,height*0.25);
			if(k.uMsg==WM_LBUTTONDOWN) 
			{
				clearrectangle(w*0.4,height*0.41,w*0.5,height*0.45);
				putimage(w*0.4,height*0.41, w*0.4, height*0.3, &background, w*0.4, height*0.41);
				outtextxy(w*0.4, height*0.41, sub[1]);
				strcpy(a.subject,sub[1]);
			}
		}
		if(!(k.x>=w*0&&k.x<=w*0.2&&k.y>=height*0.21&&k.y<=height*0.25))
		{
			setlinecolor(TRANSPARENT);
			rectangle(w*0,height*0.21,w*0.2,height*0.25);
		}
		if(k.x>=w*0&&k.x<=w*0.2&&k.y>=height*0.31&&k.y<=height*0.35)
		{
			setlinecolor(RED);
			rectangle(w*0,height*0.31,w*0.2,height*0.35);
			if(k.uMsg==WM_LBUTTONDOWN) 
			{
				clearrectangle(w*0.4,height*0.41,w*0.5,height*0.45);
				putimage(w*0.4,height*0.41, w*0.4, height*0.3, &background, w*0.4, height*0.41);
				outtextxy(w*0.4, height*0.41, sub[2]);
				strcpy(a.subject,sub[2]);
			}
		}
		if(!(k.x>=w*0&&k.x<=w*0.2&&k.y>=height*0.31&&k.y<=height*0.35))
		{
			setlinecolor(TRANSPARENT);
			rectangle(w*0,height*0.31,w*0.2,height*0.35);
		}
		if(k.x>=w*0&&k.x<=w*0.2&&k.y>=height*0.41&&k.y<=height*0.45)
		{
			setlinecolor(RED);
			rectangle(w*0,height*0.41,w*0.2,height*0.45);
			if(k.uMsg==WM_LBUTTONDOWN) 
			{
				clearrectangle(w*0.4,height*0.41,w*0.5,height*0.45);
				putimage(w*0.4,height*0.41, w*0.4, height*0.3, &background, w*0.4, height*0.41);
				outtextxy(w*0.4, height*0.41, sub[3]);
				strcpy(a.subject,sub[3]);
			}
		}
		if(!(k.x>=w*0&&k.x<=w*0.2&&k.y>=height*0.41&&k.y<=height*0.45))
		{
			setlinecolor(TRANSPARENT);
			rectangle(w*0,height*0.41,w*0.2,height*0.45);
		}
		if(k.x>=w*0&&k.x<=w*0.2&&k.y>=height*0.51&&k.y<=height*0.55)
		{
			setlinecolor(RED);
			rectangle(w*0,height*0.51,w*0.2,height*0.55);
			if(k.uMsg==WM_LBUTTONDOWN) 
			{
				clearrectangle(w*0.4,height*0.41,w*0.5,height*0.45);
				putimage(w*0.4,height*0.41, w*0.4, height*0.3, &background, w*0.4, height*0.41);
				outtextxy(w*0.4, height*0.41, sub[4]);
				strcpy(a.subject,sub[4]);
			}
		}
		if(!(k.x>=w*0&&k.x<=w*0.2&&k.y>=height*0.51&&k.y<=height*0.55))
		{
			setlinecolor(TRANSPARENT);
			rectangle(w*0,height*0.51,w*0.2,height*0.55);
		}
		//
		////////////////////////////////////////////////////////////
		////////////////////////////////
		if(k.x>=w*0.1&&k.x<=w*0.25&&k.y>=height*0.8&&k.y<=height*0.85)
		{
			setlinecolor(RED);
			rectangle(w*0.1,height*0.8,w*0.25,height*0.85);
			if(k.uMsg==WM_LBUTTONDOWN) 
			{
				Form1(w,h);
			}
		}
		if(!(k.x>=w*0.1&&k.x<=w*0.25&&k.y>=height*0.8&&k.y<=height*0.85))
		{
			setlinecolor(WHITE);
			rectangle(w*0.1,height*0.8,w*0.25,height*0.85);
		}
		if(k.x>=w*0.7&&k.x<=w*0.85&&k.y>=height*0.8&&k.y<=height*0.85)
		{
			setlinecolor(RED);
			rectangle(w*0.7,height*0.8,w*0.85,height*0.85);
			if(k.uMsg==WM_LBUTTONDOWN) 
			{
				ofstream outfile;
				LinkInsert(stu,1,a);
				length=ListLength(stu);
				write(stu,length);
				ofstream output;
				output.open("data.txt");
				output<<length;
				output.close();
				outtextxy(w*0.4,height*0.81,_T("添加成功"));

			}

		}
		if(!(k.x>=w*0.7&&k.x<=w*0.85&&k.y>=height*0.8&&k.y<=height*0.85))
		{
			setlinecolor(WHITE);
			rectangle(w*0.7,height*0.8,w*0.85,height*0.85);
		}

 	}

}
void Form3(int w,int h)//查询
{
	ifstream in;in.open("data.txt");
	int n;in>>n;in.close();read(stu,n);
    ///////////////
	int i=h/100 ,nd;char nb[50];
	int height=i*100;
	HWND hwnd=initgraph(w, height);
	SetWindowTextA(hwnd, "考试报名管理系统");
	IMAGE background;
	loadimage(&background, "images/003.jpg");
	putimage(0,0,&background);
	settextstyle(30, 0, _T("华文楷体"));
	setbkmode(TRANSPARENT);
	outtextxy(w*0.3,height*0.11,_T("查询："));
	outtextxy(w*0.3,height*0.21,_T("学号："));
	outtextxy(w*0.3,height*0.31,_T("姓名："));
	outtextxy(w*0.3,height*0.41,_T("性别："));
	outtextxy(w*0.3,height*0.51,_T("学科："));
	rectangle(w*0.1,height*0.8,w*0.25,height*0.85);
	outtextxy(w*0.15,height*0.81,_T("返回"));
	rectangle(w*0.7,height*0.8,w*0.85,height*0.85);
	outtextxy(w*0.75,height*0.81,_T("确定"));
	InputBox(nb, 50, _T("请输入\n查询的姓名或者位置:"),false);
	outtextxy(w*0.4, height*0.11, nb);
	MOUSEMSG k;
	while(true)
	{
		k=GetMouseMsg();
		if(k.x>=w*0.1&&k.x<=w*0.25&&k.y>=height*0.8&&k.y<=height*0.85)
		{
			setlinecolor(RED);
			rectangle(w*0.1,height*0.8,w*0.25,height*0.85);
			if(k.uMsg==WM_LBUTTONDOWN) 
			{
				Form1(w,h);
			}
		}
		if(!(k.x>=w*0.1&&k.x<=w*0.25&&k.y>=height*0.8&&k.y<=height*0.85))
		{
			setlinecolor(WHITE);
			rectangle(w*0.1,height*0.8,w*0.25,height*0.85);
		}
		if(k.x>=w*0.7&&k.x<=w*0.85&&k.y>=height*0.8&&k.y<=height*0.85)
		{
			setlinecolor(RED);
			rectangle(w*0.7,height*0.8,w*0.85,height*0.85);
			if(k.uMsg==WM_LBUTTONDOWN) 
			{
				if(isDigit(nb))
				{
					nd=atoi(nb);
					if(!GetElem(stu,nd))
						outtextxy(w*0.4, height*0.81, "不存在此学生！！！");
					else
					{
						Sleep(100);
						outtextxy(w*0.4, height*0.21, GetElem(stu,nd)->data.num);
						Sleep(100);
						outtextxy(w*0.4, height*0.31,GetElem(stu,nd)->data.name);
						Sleep(100);
						outtextxy(w*0.4, height*0.41, GetElem(stu,nd)->data.gender);
						Sleep(100);
						outtextxy(w*0.4, height*0.51, GetElem(stu,nd)->data.subject);
					}
				}
				else
				{
					if(!GetName(stu,nb))
						outtextxy(w*0.4, height*0.81, "不存在此学生！！！");
					else
					{
						Sleep(100);
						outtextxy(w*0.4, height*0.21, GetName(stu,nb)->data.num);
						Sleep(100);
						outtextxy(w*0.4, height*0.31,GetName(stu,nb)->data.name);
						Sleep(100);
						outtextxy(w*0.4, height*0.41, GetName(stu,nb)->data.gender);
						Sleep(100);
						outtextxy(w*0.4, height*0.51, GetName(stu,nb)->data.subject);
					}

				}
			}

		}
		if(!(k.x>=w*0.7&&k.x<=w*0.85&&k.y>=height*0.8&&k.y<=height*0.85))
		{
			setlinecolor(WHITE);
			rectangle(w*0.7,height*0.8,w*0.85,height*0.85);
		}
	}
}
void Form4(int w,int h)//删除
{
	ifstream in;in.open("data.txt");
	int n;in>>n;in.close();
	read(stu,n);
	int i=h/100 ,nd;char nb[10];
	int height=i*100;
	HWND hwnd=initgraph(w, height);
	SetWindowTextA(hwnd, "考试报名管理系统");
	IMAGE background;
	loadimage(&background, "images/004.jpg");
	putimage(0,0,&background);
	settextstyle(30, 0, _T("华文楷体"));
	setbkmode(TRANSPARENT);
	outtextxy(w*0.3,height*0.11,_T("删除："));
	outtextxy(w*0.3,height*0.21,_T("学号："));
	outtextxy(w*0.3,height*0.31,_T("姓名："));
	outtextxy(w*0.3,height*0.41,_T("性别："));
	outtextxy(w*0.3,height*0.51,_T("学科："));
	rectangle(w*0.1,height*0.8,w*0.25,height*0.85);
	outtextxy(w*0.15,height*0.81,_T("返回"));
	rectangle(w*0.7,height*0.8,w*0.85,height*0.85);
	outtextxy(w*0.75,height*0.81,_T("确定"));
	InputBox(nb, 50, _T("请输入要删除的姓名:"),false);
	outtextxy(w*0.4, height*0.11, nb);
	
	MOUSEMSG k;
	while(true)
	{
		k=GetMouseMsg();
		if(k.x>=w*0.1&&k.x<=w*0.25&&k.y>=height*0.8&&k.y<=height*0.85)
		{
			setlinecolor(RED);
			rectangle(w*0.1,height*0.8,w*0.25,height*0.85);
			if(k.uMsg==WM_LBUTTONDOWN) 
			{
				Form1(w,h);
			}
		}
		if(!(k.x>=w*0.1&&k.x<=w*0.25&&k.y>=height*0.8&&k.y<=height*0.85))
		{
			setlinecolor(WHITE);
			rectangle(w*0.1,height*0.8,w*0.25,height*0.85);
		}
		if(k.x>=w*0.7&&k.x<=w*0.85&&k.y>=height*0.8&&k.y<=height*0.85)
		{
			setlinecolor(RED);
			rectangle(w*0.7,height*0.8,w*0.85,height*0.85);
			if(k.uMsg==WM_LBUTTONDOWN) 
			{
				if(!GetName(stu,nb))
					outtextxy(w*0.4, height*0.81, "不存在此学生！！！");
				else
				{
					Sleep(100);
					outtextxy(w*0.4, height*0.21, GetName(stu,nb)->data.num);
					Sleep(100);
					outtextxy(w*0.4, height*0.31,GetName(stu,nb)->data.name);
					Sleep(100);
					outtextxy(w*0.4, height*0.41, GetName(stu,nb)->data.gender);
					Sleep(100);
					outtextxy(w*0.4, height*0.51, GetName(stu,nb)->data.subject);
					//MessageBox(hwnd,"确认删除！！！","提示",MB_OK);
					DeleteLists(stu,nb);
					length=ListLength(stu);
					write(stu,length);
					ofstream output;
					output.open("data.txt");
					output<<length;
					output.close();
					outtextxy(w*0.4, height*0.81, "删除成功");
				}

			}
		}
		if(!(k.x>=w*0.7&&k.x<=w*0.85&&k.y>=height*0.8&&k.y<=height*0.85))
		{
			setlinecolor(WHITE);
			rectangle(w*0.7,height*0.8,w*0.85,height*0.85);
		}
	}
}
void Form5(int w,int h)//统计
{
	ifstream in;in.open("data.txt");
	int n;in>>n;in.close();
	read(stu,n);
	LinkList p=stu->next;
	int number=1,b=0;
	int i=h/100 ,nd;char nb[10];
	int height=i*100;
	int page = (n / 7)+1;
	int single_page = 1;
	student *stud=new student[100];
	HWND hwnd=initgraph(w, height);
	SetWindowTextA(hwnd, "考试报名管理系统");
	IMAGE background;
	loadimage(&background, "images/005.jpg");
	putimage(0,0,&background);
	settextstyle(30, 0, _T("华文楷体"));
	setbkmode(TRANSPARENT);
	settextcolor(BLACK);
	rectangle(w * 0.3, height * 0.8, w * 0.35, height * 0.85);
	outtextxy(w * 0.3, height * 0.81, _T("<<"));
	char page_length[10];
	sprintf_s(page_length, "%d", page);
	outtextxy(w * 0.39, height * 0.81, "/");
	outtextxy(w * 0.41, height * 0.81, page_length);//总页数
	char single_length[10];
	sprintf_s(single_length, "%d", single_page);
	outtextxy(w * 0.37, height * 0.81, single_length);//单页数

	rectangle(w * 0.45, height * 0.8, w * 0.5, height * 0.85);
	outtextxy(w * 0.45, height * 0.81, _T(">>"));
	rectangle(w*0.3,height*0.9,w*0.5,height*0.95);
	outtextxy(w*0.37,height*0.91,_T("返回"));
	while(p!=NULL)
	{
		strcpy(stud[b].num, p->data.num);
		strcpy(stud[b].name, p->data.name);
		strcpy(stud[b].gender, p->data.gender);
		strcpy(stud[b].subject, p->data.subject);
		b++;
		p = p->next;
	}
	for (b=0; b < 7; b++) {
		outtextxy(w * 0.1, height * 0.10 * number, stud[b].num);
		outtextxy(w * 0.25, height * 0.10 * number, stud[b].name);
		outtextxy(w * 0.35, height * 0.10 * number, stud[b].gender);
		outtextxy(w * 0.4, height * 0.10 * number, stud[b].subject);
		number++;
	}
	MOUSEMSG k;
	while(true)
	{
		k=GetMouseMsg();
		
		if(k.x>=w*0.3&&k.x<=w*0.5&&k.y>=height*0.9&&k.y<=height*0.95)
		{
			setlinecolor(RED);
			rectangle(w*0.3,height*0.9,w*0.5,height*0.95);
			if(k.uMsg==WM_LBUTTONDOWN) 
				Form1(w,h);
		}
		if(!(k.x>=w*0.3&&k.x<=w*0.5&&k.y>=height*0.9&&k.y<=height*0.95))
		{
			setlinecolor(WHITE);
			rectangle(w*0.3,height*0.9,w*0.5,height*0.95);
		}
		if (k.x >= w * 0.3 && k.x <= w * 0.35 && k.y >= height * 0.8 && k.y <= height * 0.85)
		{
			setlinecolor(RED);
			rectangle(w * 0.3, height * 0.8, w * 0.35, height * 0.85);
			if (k.uMsg == WM_LBUTTONDOWN)
			{
				clearrectangle(0, 0, w, height * 0.8);
				putimage(0, 0, w, height * 0.8+1, &background, 0, 0);

				if (single_page == 1)
				{
					number = 1;
					single_page = 1;
					for (int a = single_page - 1; a < 7; a++) {
						outtextxy(w*0.1,height*0.10*number, stud[a].num);
						outtextxy(w*0.25,height*0.10*number, stud[a].name);
						outtextxy(w*0.35,height*0.10*number, stud[a].gender);
						outtextxy(w*0.4,height*0.10*number, stud[a].subject);
						number++;
						
					}
				}
					
				else
				{
					number = 1;
					single_page--;
					for (int a = single_page*7 - 7; a < single_page * 7; a++) {
						outtextxy(w * 0.1, height * 0.10 * number, stud[a].num);
						outtextxy(w * 0.25, height * 0.10 * number, stud[a].name);
						outtextxy(w * 0.35, height * 0.10 * number, stud[a].gender);
						outtextxy(w * 0.4, height * 0.10 * number, stud[a].subject);
						number++;
					}
					
				}
				clearrectangle(w * 0.37, height * 0.81, w * 0.37 + textwidth(single_page), height * 0.81 + textheight(single_page));
				putimage(w * 0.37, height * 0.81, textwidth(single_page)+4 , textheight(single_page) + 2, &background, w * 0.37, height * 0.81);
				sprintf_s(single_length, "%d", single_page);
				outtextxy(w * 0.37, height * 0.81, single_length);//单页数
			}
		}
		if (!(k.x >= w * 0.3 && k.x <= w * 0.35 && k.y >= height * 0.8 && k.y <= height * 0.85))
		{
			setlinecolor(WHITE);
			rectangle(w * 0.3, height * 0.8, w * 0.35, height * 0.85);
		}
		if (k.x >= w * 0.45 && k.x <= w * 0.5 && k.y >= height * 0.8 && k.y <= height * 0.85)
		{
			setlinecolor(RED);
			rectangle(w * 0.45, height * 0.8, w * 0.5, height * 0.85);
			if (k.uMsg == WM_LBUTTONDOWN)
			{
				clearrectangle(0, 0, w, height * 0.8);
				putimage(0, 0, w, height * 0.8+1, &background, 0, 0);
				if (single_page == page)
				{
					number = 1;
					single_page = page;
					for (int a = single_page * 7 - 7; a < n; a++) {
						outtextxy(w * 0.1, height * 0.10 * number, stud[a].num);
						outtextxy(w * 0.25, height * 0.10 * number, stud[a].name);
						outtextxy(w * 0.35, height * 0.10 * number, stud[a].gender);
						outtextxy(w * 0.4, height * 0.10 * number, stud[a].subject);
						number++;
					}
				}

				else
				{
					number = 1;
					single_page++;
					for (int a = single_page * 7 - 7; a < n; a++) {
						outtextxy(w * 0.1, height * 0.10 * number, stud[a].num);
						outtextxy(w * 0.25, height * 0.10 * number, stud[a].name);
						outtextxy(w * 0.35, height * 0.10 * number, stud[a].gender);
						outtextxy(w * 0.4, height * 0.10 * number, stud[a].subject);
						number++;
					}

				}
				clearrectangle(w * 0.37, height * 0.81, w*0.37+textwidth(single_page), height * 0.81+textheight(single_page));
				putimage(w * 0.37, height * 0.81, textwidth(single_page)+1, textheight(single_page)+1, &background, w * 0.37, height * 0.81);
				sprintf_s(single_length, "%d", single_page);
				outtextxy(w * 0.37, height * 0.81, single_length);//单页数
			}
		}
		if (!(k.x >= w * 0.45 && k.x <= w * 0.5 && k.y >= height * 0.8 && k.y <= height * 0.85))
		{
			setlinecolor(WHITE);
			rectangle(w * 0.45, height * 0.8, w * 0.5, height * 0.85);
		}

	}
}
void Form (int w,int h)//登录
{
	//char inName[10],onName[10];
	string inName, onName;
	IMAGE background;
	loadimage(&background, "images/deng.jpg");
	int width=background.getwidth();
	int length=background.getheight();
	HWND hwnd=initgraph(width, length);
	SetWindowTextA(hwnd, "考试报名管理系统");
	putimage(0,0,&background);
	settextstyle(30, 0, _T("华文楷体"));
	setbkmode(TRANSPARENT);
	settextcolor(BLACK);
	outtextxy(width*0.08, length*0.08, _T("账号:"));
	outtextxy(width*0.08, length*0.2, _T("密码:"));
	rectangle(width*0.2,length*0.08,width*0.6,length*0.15);
	rectangle(width*0.2,length*0.2,width*0.6,length*0.27);

	rectangle(width*0.08,length*0.8,width*0.3,length*0.9);
	rectangle(width*0.7,length*0.8,width*0.92,length*0.9);
	setcolor(BLACK);//填充颜色
	setfillcolor(YELLOW);//边框颜色

	outtextxy(width*0.14,length*0.81,_T("取消"));
	outtextxy(width*0.75,length*0.81,_T("确定"));
	//InputBox(inName, 10, "请输入账号:",false);
	//outtextxy(width*0.2, length*0.08, inName.c_str());
	//Sleep(500);
	//InputBox(onName, 10, "请输入密码:",false);
	//outtextxy(width*0.2, length*0.2, onName.c_str());
	/////////////创建文件
	ofstream outfile("exam.txt",ios::in);
	if(!outfile)
	{
		outfile.close();
		outfile.open("exam.txt",ios::trunc);
		outfile<<"";
	}
	outfile.close();
	ofstream out("data.txt",ios::in);
	if(!out)
	{
		out.close();
		out.open("data.txt",ios::trunc);
		out<<0;
	}
	out.close();
	////////////////////////
	MOUSEMSG k;
	while(true)
	{
		k=GetMouseMsg();
		
		if (k.x > width * 0.2 && k.x<width * 0.6 && k.y>length * 0.08 && k.y < length * 0.15)
		{
			setlinecolor(RED);
			rectangle(width * 0.2, length * 0.08, width * 0.6, length * 0.15);
			Input(inName, width * 0.2, length * 0.08, k, background, "images/deng.jpg");
		}
		if (!(k.x > width * 0.2 && k.x<width * 0.6 && k.y>length * 0.08 && k.y < length * 0.15))
		{
			setlinecolor(WHITE);
			rectangle(width * 0.2, length * 0.08, width * 0.6, length * 0.15);
		}
		
		if (k.x > width * 0.2 && k.x<width * 0.6 && k.y>length * 0.2 && k.y < length * 0.27)
		{
			setlinecolor(RED);
			rectangle(width * 0.2, length * 0.2, width * 0.6, length * 0.27);
			Input(onName, width * 0.2, length * 0.2, k, background, "images/deng.jpg");
		}
		if (!(k.x > width * 0.2 && k.x<width * 0.6 && k.y>length * 0.2 && k.y < length * 0.27))
		{
			setlinecolor(WHITE);
			rectangle(width * 0.2, length * 0.2, width * 0.6, length * 0.27);
		}
		if(k.x>=width*0.08&&k.x<=width*0.3&&k.y>=length*0.8&&k.y<=length*0.9)
		{
			setlinecolor(RED);
			rectangle(width*0.08,length*0.8,width*0.3,length*0.9);
			if(k.uMsg==WM_LBUTTONDOWN)
				exit(0);
		}
		if(!(k.x>=width*0.08&&k.x<=width*0.3&&k.y>=length*0.8&&k.y<=length*0.9))
		{
			setlinecolor(WHITE);
			rectangle(width*0.08,length*0.8,width*0.3,length*0.9);
		}
		if(k.x>=width*0.7&&k.x<=width*0.92&&k.y>=length*0.8&&k.y<=length*0.9)
		{
			setlinecolor(RED);
			rectangle(width*0.7,length*0.8,width*0.92,length*0.9);
			if(k.uMsg==WM_LBUTTONDOWN)
			{
				if(inName=="admin" && onName=="123456")
					Form1(w,h);
				else
				{
					MessageBox(hwnd, "账号密码错误！！", "提示", NULL);
				}
			}

		}
		if(!(k.x>=width*0.7&&k.x<=width*0.92&&k.y>=length*0.8&&k.y<=length*0.9))
		{
			setlinecolor(WHITE);
			rectangle(width*0.7,length*0.8,width*0.92,length*0.9);
		}
	}
}
void GetIMEString(HWND hWnd, string& str)
{
	HIMC hIMC = ImmGetContext(hWnd);//获取HIMC 
	if (hIMC)
	{
		//这里先说明一下，以输入“中国”为例 
		//切换到中文输入法后，输入“zhongguo”，这个字符串称作IME组成字符串 
		//而在输入法列表中选择的字符串“中国”则称作IME结果字符串 
		static bool flag = false;//输入完成标记：在输入中时，IME组成字符串不为空，置true；输入完成后，IME组成字符串为空，置false 
		DWORD dwSize = ImmGetCompositionStringW(hIMC, GCS_COMPSTR, NULL, 0); //获取IME组成输入的字符串的长度 
		if (dwSize > 0)//如果IME组成字符串不为空，且没有错误（此时dwSize为负值），则置输入完成标记为true 
		{
			if (flag == false)
			{
				flag = true;
			}
		}
		else if (dwSize == 0 && flag) //如果IME组成字符串为空，并且标记为true，则获取IME结果字符串 
		{
			int iSize; //IME结果字符串的大小 
			LPSTR pszMultiByte = NULL;//IME结果字符串指针 
			int ChineseSimpleAcp = 936;//宽字节转换时中文的编码 
			WCHAR* lpWideStr = NULL;//宽字节字符数组 
			dwSize = ImmGetCompositionStringW(hIMC, GCS_RESULTSTR, NULL, 0);//获取IME结果字符串的大小 
			if (dwSize > 0) //如果IME结果字符串不为空，且没有错误 
			{
				dwSize += sizeof(WCHAR);//大小要加上NULL结束符 
				//为获取IME结果字符串分配空间 
				if (lpWideStr)
				{
					delete[]lpWideStr;
					lpWideStr = NULL;
				}
				lpWideStr = new WCHAR[dwSize];
				memset(lpWideStr, 0, dwSize); //清空结果空间 
				ImmGetCompositionStringW(hIMC, GCS_RESULTSTR, lpWideStr, dwSize);//获取IME结果字符串，这里获取的是宽字节 
				iSize = WideCharToMultiByte(ChineseSimpleAcp, 0, lpWideStr, -1, NULL, 0, NULL, NULL);//计算将IME结果字符串转换为ASCII标准字节后的大小 
				//为转换分配空间 
				if (pszMultiByte)
				{
					delete[] pszMultiByte;
					pszMultiByte = NULL;
				}
				pszMultiByte = new char[iSize + 1];
				WideCharToMultiByte(ChineseSimpleAcp, 0, lpWideStr, -1, pszMultiByte, iSize, NULL, NULL);//宽字节转换 
				pszMultiByte[iSize] = '\0';
				str += pszMultiByte;//添加到string中 
				//释放空间 
				if (lpWideStr)
				{
					delete[]lpWideStr;
					lpWideStr = NULL;
				}
				if (pszMultiByte)
				{
					delete[] pszMultiByte;
					pszMultiByte = NULL;
				}
			}
			flag = false;
		}
		ImmReleaseContext(hWnd, hIMC);//释放HIMC 
	}
}
void Input(string &name, int x, int y, MOUSEMSG k,IMAGE bg_img,string image_road)
{
	
	loadimage(&bg_img, image_road.c_str());
	setbkmode(TRANSPARENT);//文字背景透明
	settextstyle(30, 0, _T("华文楷体"));//文字样式 30为字号

	/*clearrectangle(x, y, x + 370, y + 40);
	putimage(x, y, 372, 42, &bg_img, x, y);
	outtextxy(x, y + 8, name.c_str());*/
	while (true)
	{
		if (MouseHit())
			k = GetMouseMsg();
		if (_kbhit())
		{

			char c = _getch();
			if (c == 8)
			{
				if (name.length() > 0)
				{
					clearrectangle(x, y, x + 370, y + 40);
					putimage(x, y, 372, 42, &bg_img, x, y);
					name.resize(name.length() - 1);
					outtextxy(x, y, name.c_str());
				}
			}
			else if (c == 13 || (!(k.x >= x && k.x <= x + 370 && k.y >= y && k.y <= y + 40))) {
				/*if (name != "")
				{
					setcolor(RED);
					clearrectangle(x + 370, y, x + 400, y + 40);
					putimage(x + 370, y, 42, 42, &bg_img, x + 370, y);
					outtextxy(x + 370, y + 8, "√");
				}
				else {
					setcolor(RED);
					clearrectangle(x + 370, y, x + 400, y + 40);
					putimage(x + 370, y, 42, 42, &bg_img, x + 370, y);
					outtextxy(x + 370, y + 8, "!");
				}
				setcolor(WHITE);*/
				break;
			}
			else if (k.x >= x && k.x <= x + 370 && k.y >= y && k.y <= y + 40)
			{
				name += c;
			}

		}
		if (textwidth(name.c_str()) >= 370)
		{
			clearrectangle(x, y, x + 370, y + 40);
			putimage(x, y, 372, 42, &bg_img, x, y);
			name.resize(name.length() - 1);
		}
		outtextxy(x, y , name.c_str());
		FlushBatchDraw();


	}
}
void Input(HWND hwnd, string& name, int x, int y, IMAGE bg_img, string image_road)
{
	loadimage(&bg_img, image_road.c_str());
	setbkmode(TRANSPARENT);//文字背景透明
	settextstyle(30, 0, _T("华文楷体"));//文字样式 30为字号

	//clearrectangle(x, y, x + 370, y + 40);
	////putimage(x, y, 372, 42, &bg_img, x, y);
	//outtextxy(x, y , name.c_str());
	while (true)
	{
		if (_kbhit())
		{

			char c = _getch();
			if (c == 8)
			{
				if (name.length() > 0)
				{
					clearrectangle(x, y, x + 370, y + 40);
					putimage(x, y, 372, 42, &bg_img, x, y);
					if (name.at(name.length() - 1) & 0x8000)
						name.erase(name.end() - 1);
					name.erase(name.end() - 1);
				}
			}
			else if (c == 13) {
				//if (name != "")
				//{
				//	setcolor(RED);
				//	clearrectangle(x + 370, y, x + 400, y + 40);
				//	putimage(x + 370, y, 42, 42, &bg_img, x + 370, y);
				//	outtextxy(x + 370, y + 8, "√");
				//}
				//else {
				//	setcolor(RED);
				//	clearrectangle(x + 370, y, x + 400, y + 40);
				//	//putimage(x + 370, y, 42, 42, &bg_img, x + 370, y);
				//	outtextxy(x + 370, y + 8, "!");
				//}
				//y += 40;
				//setcolor(WHITE);
				break;
			}
			else 
			{
				name += c;
			}

		}
		else //除此之外，检测是否有IME输入，如果有，则将输入结果添加到string中 
		{
			GetIMEString(hwnd, name);
		}
		//if (textwidth(name.c_str()) >= 370)
		//{
		//	clearrectangle(x, y, x + 370, y + 40);
		//	putimage(x, y, 372, 42, &bg_img, x, y);
		//	//name.resize(name.length() - 1);
		//	if (name.at(name.length() - 1) & 0x8000)
		//		name.erase(name.end() - 1);
		//	name.erase(name.end() - 1);
		//}
		outtextxy(x, y+8 , name.c_str());
		FlushBatchDraw();


	}
}