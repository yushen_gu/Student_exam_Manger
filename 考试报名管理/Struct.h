#include<fstream>
#include<string>
#include<iostream>
using namespace std;
#define OK 1
#define ERROR 0
typedef int Status;
typedef struct student{
	char num[10];
	char name[50];
	char gender[6];
	char subject[100];
}ElemType;
typedef struct LNode{
	ElemType data;
	LNode *next;
}*LinkList;

istream &operator >>(istream &input ,ElemType &a)
{
	cout<<"请输入 学号、姓名、性别、年龄"<<endl;
	input>>a.num>>a.name>>a.gender>>a.subject;
	return input;
}
ostream &operator <<(ostream &output,ElemType &a)
{
	cout<<'\t'<<"学号"<<'\t'<<"姓名"<<'\t'<<"性别"<<'\t'<<"年龄"<<endl;
	output<<'\t'<<a.num<<'\t'<<a.name<<'\t'<<a.gender<<'\t'<<a.subject<<endl;
	return output;
}

Status InitList_L(LinkList &L)//初始化
{
	L=new LNode;
	L->next=NULL;
	return OK;
}
Status DestroyList_L(LinkList &L)//销毁
{
	LinkList P;
	while(L)
	{
		P=L;
		L=L->next;
		delete P;
	}
	
	return OK;
}
Status ClearList_L(LinkList &L)//清空
{
	LinkList p,q;
	p=L->next;
	while(p)
	{
		q=p->next;
		delete p;
		p=q;
	}
	L->next=NULL;
	return OK;
}
bool ListEmpty(LinkList L)//判空
{
	if(L->next)
		return false;
	else
		return true;
}
int ListLength(LinkList &L)//长度
{
	LinkList p=L->next;
	int i=0;
	while(p)
	{
		i++;
		p=p->next;
	}
	return i;
}
LinkList GetName(LinkList L,char name[50])//查找名字
{
	
	LinkList p=L->next;
	while(p&&(strcmp(p->data.name,name)!=0))
		p=p->next;
	if(p==NULL)
	{
		return NULL;
	}
	return p;
}
LinkList GetElem(LinkList L,int i)//查找位置
{
	
	LinkList p=L->next;int j=1;
	while(p&&j<i)
	{
		p=p->next;
		++j;
	}
	if(!p||j>i)
		return NULL;
	return p;
}
int LocateElem(LinkList L,string e)//根据姓名查找位置
{
	LinkList p;int i=1;
	p=L->next;
	while (p&&p->data.name!=e)
	{
		p=p->next;
		i++;
	}
	if(p)
	{
		cout<<e<<"是第"<<i<<"个位置"<<endl;
		return i;
	}
	else
		return 0;
}
//返回地址
LNode* LocateElem_L(LinkList &L,string e)
{
	LinkList p;
	p=L->next;
	while (p&&p->data.name!=e)
		p=p->next;
	return p;
}		
Status LinkInsert(LinkList &L,int i,ElemType e)//插入
{
	LinkList s,p=L;int j=0;
	while(p&&j<i-1)
	{
		p=p->next;
		++j;
	}
	if(!p||j>i-1)
		return ERROR;
	s=new LNode;
	s->data=e;
	s->next=p->next;
	p->next=s;
	return OK;
}
Status DeleteLists(LinkList &L,char name[50])//查找姓名删除
{
	int i=1;
	LinkList p=L,q;
	while(p&&(strcmp(p->next->data.name,name)!=0))
	{
		if(i!=1)
			p=p->next;
		i=0;
	}
	if(!p->next)
		return ERROR;
	q=p->next;
	p->next=q->next;
	delete q;
	return OK;
}
Status DeleteList(LinkList &L,int i)//查找位置删除
{
	int j=0;
	LinkList p=L,q;
	while(j<j-1&&p->next)
	{
		j++;
		p=p->next;
	}
	if(!p->next||j>i-1)
		return ERROR;
	q=p->next;
	/*e=q->data;*/
	p->next=q->next;
	delete q;
	cout<<"删除成功！"<<endl;
	return OK;
}
//头插法
void CreateList_H(LinkList &L,int n)
{
	LinkList p=NULL;
	for(int i=0;i<n;++i)
	{

		p=new LNode;
		cout<<"请输入第"<<i+1<<"个"<<endl;
		cin>>p->data;
		p->next=L->next;
		L->next=p;
		
	}
}
//尾插法
void CreateList_E(LinkList &L,int n)
{
	LinkList p,r=L;
	for(int i=0;i<n;i++)
	{
		p=new LNode;
		cout<<"请输入第"<<i+1<<"个"<<endl;
		cin>>p->data;
		p->next=NULL;
		r->next=p;
		r=p;
	}
}
void PrintList(LinkList &L)//输出
{
	LinkList p=L->next;
	while(p!=NULL)
	{
		cout<<p->data<<endl;
		p=p->next;
	}
}
void add(LinkList &L)//添加
{
	int n;
	cout<<"请输入要添加的个数：";
	cin>>n;
	while(n)
		if(n<0)
		{
			cout<<"数据有误！请重新输入！！！"<<endl;
			cin>>n;
		}
		else
			break;
	CreateList_E(L,n);
}

Status read(LinkList &L,int n)//尾插法读取文件
{
	ifstream infile;
	infile.open("exam.txt");
	LinkList p=NULL,r=L;
	for(int i=0;i<n;++i)
	{

		p=new LNode;
		infile>>p->data.num>>p->data.name>>p->data.gender>>p->data.subject;
		p->next=NULL;
		r->next=p;
		r=p;
	}
	infile.close();
	return OK;
}
Status write(LinkList &L,int n)//写入文件
{
	ofstream outfile1("exam.txt",ios::out);
	outfile1.close();
	ofstream outfile;
	outfile.open("exam.txt");
	LinkList p=NULL;
	
	for(int i=0;i<n;++i)
	{   
		L=L->next;
		outfile<<L->data.num<<'\t'<<L->data.name<<'\t'<<L->data.gender<<'\t'<<L->data.subject<<endl;
	}
	outfile.close();
	return OK;
}
bool isDigit(char charArray[]) //判断数组内容是否为全数字
{
	int count = 0;
	for (int i = 0; i < strlen(charArray); i++) {
		if (charArray[i] >= 48 && charArray[i] <= 57) {
			count++;
		}
	}
	if (count == strlen(charArray)) {
		return true;
	} else {
		return false;
	}
}
Status SortList(LinkList &L)//对链表采用插入排序
{
	LinkList q=L->next,head,s=L;head=s;
	student sort[100];
	int i,j,k=1;
	while(q)
	{
		sort[k]=q->data;
		q=q->next;
		k=k+1;
	}
	for(i=2;i<=k-1;i++)
	{
		if(atoi(sort[i].num)<atoi(sort[i-1].num))
		{

			sort[0]=sort[i];
			sort[i]=sort[i-1];
			for(j=i-2;atoi(sort[0].num)<atoi(sort[j].num);--j)
				sort[j+1]=sort[j];
			sort[j+1]=sort[0];
		}
	}
	s=s->next;
	for(i=1;i<=k-1;i++)
	{
		s->data=sort[i];
		s=s->next;
	}
	L=head;
	return OK;
}